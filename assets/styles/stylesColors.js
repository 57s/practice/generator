const COLORS = {
	white: 'white',
	black: 'black',
	// light: '',
	// dark: '',
	accent: '#ff5832',

	bg: '#181818',
	bg_dark: '#161516',
	// bg_light: '',
};

export default COLORS;
