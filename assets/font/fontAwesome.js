import {
	faTrash,
	faClipboard,
	faBrush,
	faMinus,
	faPlus,
	faCircleArrowUp,
	faCircleArrowDown,
	faCircleXmark,
} from '@fortawesome/free-solid-svg-icons';
import { library } from '@fortawesome/fontawesome-svg-core';

library.add(
	faTrash,
	faClipboard,
	faBrush,
	faMinus,
	faPlus,
	faCircleArrowUp,
	faCircleArrowDown,
	faCircleXmark
);
