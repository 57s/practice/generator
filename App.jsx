import RouterProvider from './src/providers/routes/RouterProvider.jsx';
import { AppProvider } from './src/providers/app/appProvider.jsx';
import './assets/font/fontAwesome.js';
import { StatusBar } from 'expo-status-bar';

function App() {
	return (
		<AppProvider>
			<RouterProvider />
			<StatusBar style="auto" />
		</AppProvider>
	);
}

export default App;
