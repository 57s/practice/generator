import React from 'react';
import { TextInput, View, TouchableOpacity } from 'react-native';

import useStyleCounter from './useStyleCounter';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import COLORS from '../../../assets/styles/stylesColors';

function Counter({ count, onChange, onIncrement, onDecrement }) {
	const styles = useStyleCounter();
	return (
		<View style={styles.counter}>
			<TouchableOpacity style={styles.button} onPress={onDecrement}>
				<FontAwesomeIcon icon="minus" color={COLORS.white} />
			</TouchableOpacity>

			<TextInput
				style={styles.count}
				value={String(count)}
				onChangeText={onChange}
				keyboardType="numeric"
			/>

			<TouchableOpacity style={styles.button} onPress={onIncrement}>
				<FontAwesomeIcon icon="plus" color={COLORS.white} />
			</TouchableOpacity>
		</View>
	);
}

export default Counter;
