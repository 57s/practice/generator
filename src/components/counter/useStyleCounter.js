import { StyleSheet } from 'react-native';
import COLORS from '../../../assets/styles/stylesColors';

function useStyleCounter() {
	const styles = StyleSheet.create({
		counter: {
			flexDirection: 'row',
			borderRadius: 30,
			backgroundColor: COLORS.accent,
		},

		button: {
			justifyContent: 'center',
			paddingHorizontal: 5,
		},

		count: {
			textAlign: 'center',
			paddingHorizontal: 5,
			fontSize: 20,
			color: COLORS.white,
		},
	});

	return styles;
}

export default useStyleCounter;
