import React from 'react';
import { Text, View, TouchableOpacity } from 'react-native';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';

import COLORS from '../../../assets/styles/stylesColors';
import useStyleButtonIcon from './useStyleButtonIcon';

function ButtonIcon({ name, onPress }) {
	const styles = useStyleButtonIcon();
	return (
		<TouchableOpacity style={styles.button} onPress={onPress}>
			<FontAwesomeIcon icon={name} color={COLORS.accent} />
		</TouchableOpacity>
	);
}

export default ButtonIcon;
