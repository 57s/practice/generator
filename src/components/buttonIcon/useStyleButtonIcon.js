import { StyleSheet } from 'react-native';

function useStyleButtonIcon() {
	const styles = StyleSheet.create({
		button: {
			padding: 7,
		},
	});

	return styles;
}

export default useStyleButtonIcon;
