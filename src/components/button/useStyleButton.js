import { StyleSheet } from 'react-native';
import COLORS from '../../../assets/styles/stylesColors';

function useStyleButton() {
	const styles = StyleSheet.create({
		button: {
			padding: 10,
			borderRadius: 8,
			backgroundColor: COLORS.accent,
			elevation: 7,
		},

		title: {
			color: COLORS.black,
			fontSize: 18,
			fontWeight: '800',
			textAlign: 'center',
		},
	});

	return styles;
}

export default useStyleButton;
