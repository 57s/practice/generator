import { useContext } from 'react';
import { Text, View } from 'react-native';

import { AppContext } from '../../providers/app/appProvider';
import useStyleLayout from './useStyleLayout';
import { SafeAreaView } from 'react-native-safe-area-context';

function Layout({ children }) {
	const { bgColor } = useContext(AppContext);
	const styles = useStyleLayout(bgColor);
	return <SafeAreaView style={styles.layout}>{children}</SafeAreaView>;
}

export default Layout;
