import { StyleSheet } from 'react-native';
import COLORS from '../../../assets/styles/stylesColors';

function useStyleLayout(bgColor) {
	const styles = StyleSheet.create({
		layout: {
			flex: 1,
			paddingHorizontal: 30,
			backgroundColor: bgColor,
		},
	});

	return styles;
}

export default useStyleLayout;
