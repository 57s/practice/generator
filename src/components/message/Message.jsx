import React from 'react';
import { Text, View } from 'react-native';

import useStyleMessage from './useStyleMessage';
import ButtonIcon from '../buttonIcon/ButtonIcon';

function Message({ text, onClose }) {
	const styles = useStyleMessage();
	setTimeout(onClose, 1500);
	return (
		<View style={styles.message}>
			<View style={styles.main}>
				<Text style={styles.text}>{text}</Text>
			</View>

			<View style={styles.control}>
				<ButtonIcon name="circle-xmark" onPress={onClose} />
			</View>
		</View>
	);
}

export default Message;
