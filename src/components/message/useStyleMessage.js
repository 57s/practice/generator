import { StyleSheet } from 'react-native';
import COLORS from '../../../assets/styles/stylesColors';

function useStyleMessage() {
	const styles = StyleSheet.create({
		message: {
			flexDirection: 'row',
			justifyContent: 'space-between',
			padding: 8,
			borderRadius: 8,
			borderWidth: 1,
			borderColor: COLORS.accent,
			elevation: 7,
			backgroundColor: COLORS.bg,
		},

		text: {
			fontSize: 20,
			color: COLORS.white,
		},
	});

	return styles;
}

export default useStyleMessage;
