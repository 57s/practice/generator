import { StyleSheet } from 'react-native';
import COLORS from '../../../assets/styles/stylesColors';

function useStyleBox() {
	const styles = StyleSheet.create({
		box: {
			justifyContent: 'center',
			alignItems: 'center',
			padding: 10,
			minHeight: 170,
			borderRadius: 15,
			borderWidth: 1,
			borderColor: COLORS.accent,
			elevation: 7,
			backgroundColor: COLORS.bg,
		},
	});

	return styles;
}

export default useStyleBox;
