import { View, TouchableOpacity } from 'react-native';

import useStyleBox from './useStyleBox';

function Box({ isShow = true, children, onPress }) {
	const styles = useStyleBox();

	if (isShow) {
		return (
			<TouchableOpacity style={styles.box} onPress={onPress}>
				{children}
			</TouchableOpacity>
		);
	}
}

export default Box;
