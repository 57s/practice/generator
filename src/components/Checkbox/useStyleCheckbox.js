import { StyleSheet } from 'react-native';
import COLORS from '../../../assets/styles/stylesColors';

function useStyleCheckbox() {
	const styles = StyleSheet.create({
		checkbox: {
			flexDirection: 'row',
			justifyContent: 'space-between',
			gap: 15,
		},

		title: {
			fontSize: 20,
			color: COLORS.white,
		},
		bouncy: {
			width: 25,
			margin: 0,
		},
	});

	return styles;
}

export default useStyleCheckbox;
