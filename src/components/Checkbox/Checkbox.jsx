import BouncyCheckbox from 'react-native-bouncy-checkbox';
import { Text, View } from 'react-native';

import useStyleCheckbox from './useStyleCheckbox';
import COLORS from '../../../assets/styles/stylesColors';

function Checkbox({ title, isChecked, onPress }) {
	const styles = useStyleCheckbox();
	return (
		<View style={styles.checkbox}>
			<Text style={styles.title}>{title}</Text>
			<BouncyCheckbox
				style={styles.bouncy}
				disableBuiltInState
				isChecked={isChecked}
				onPress={onPress}
				fillColor={COLORS.accent}
			/>
		</View>
	);
}

export default Checkbox;
