import { StyleSheet } from 'react-native';
import COLORS from '../../../assets/styles/stylesColors';

function useStyleAccordion(isOpen) {
	const styles = StyleSheet.create({
		accordion: {},

		header: {
			flexDirection: 'row',
			justifyContent: 'space-between',
			alignItems: 'center',
			paddingHorizontal: 8,
			paddingBottom: 3,
			borderTopLeftRadius: 8,
			borderTopRightRadius: 8,
			borderBottomLeftRadius: isOpen ? 0 : 8,
			borderBottomRightRadius: isOpen ? 0 : 8,
			backgroundColor: COLORS.accent,
		},

		title: {
			flex: 1,
			textAlign: 'center',
			fontSize: 24,
			fontWeight: '800',
			color: COLORS.black,
		},

		main: {
			padding: 8,
			borderBottomLeftRadius: 8,
			borderBottomRightRadius: 8,
			backgroundColor: COLORS.bg,
		},
	});

	return styles;
}

export default useStyleAccordion;
