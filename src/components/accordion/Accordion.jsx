import { Text, TouchableOpacity, View } from 'react-native';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';

import COLORS from '../../../assets/styles/stylesColors';
import ButtonIcon from '../buttonIcon/ButtonIcon';
import useStyleAccordion from './useStyleAccordion';

function Accordion({ children, title, onToggle, isOpen = true }) {
	const styles = useStyleAccordion(isOpen);
	return (
		<View style={styles.accordion}>
			<TouchableOpacity style={styles.header} onPress={onToggle}>
				<Text style={styles.title}>{title}</Text>

				<FontAwesomeIcon
					icon={isOpen ? 'circle-arrow-up' : 'circle-arrow-down'}
					color={COLORS.black}
					size={25}
				/>
			</TouchableOpacity>

			{isOpen && <View style={styles.main}>{children}</View>}
		</View>
	);
}

export default Accordion;
