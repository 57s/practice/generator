import { Text, View } from 'react-native';

import Message from '../message/Message';
import useStyleMessagesList from './useStyleMessagesList';

function MessagesList({ messages, setMessages }) {
	const styles = useStyleMessagesList();
	const removeMessage = id =>
		setMessages(messages.filter(message => message.id !== id));

	return (
		<View style={styles.list}>
			{messages.map(message => (
				<Message
					key={message.id}
					{...message}
					onClose={() => removeMessage(message.id)}
				/>
			))}
		</View>
	);
}

export default MessagesList;
