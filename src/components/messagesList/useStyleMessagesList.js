import { StyleSheet } from 'react-native';

function useStyleMessagesList() {
	const styles = StyleSheet.create({
		list: {
			gap: 15,
		},
	});

	return styles;
}

export default useStyleMessagesList;
