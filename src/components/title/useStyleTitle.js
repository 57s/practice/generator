import { StyleSheet } from 'react-native';
import { useContext } from 'react';

import COLORS from '../../../assets/styles/stylesColors';
import { AppContext } from '../../providers/app/appProvider';

function useStyleTitle() {
	const { bgColor } = useContext(AppContext);

	const styles = StyleSheet.create({
		title: {
			fontSize: 38,
			fontWeight: '800',
			color: COLORS.bg_dark === bgColor ? COLORS.accent : bgColor,
		},
	});

	return styles;
}

export default useStyleTitle;
