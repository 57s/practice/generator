import React from 'react';
import { Text, View } from 'react-native';

import useStyleTitle from './useStyleTitle';

function Title({ children }) {
	const styles = useStyleTitle();

	return <Text style={styles.title}>{children}</Text>;
}

export default Title;
