import ColorScreen from '../../pages/color/ColorScreen';
import MenuScreen from '../../pages/menu/MenuScreen';
import PasswordScreen from '../../pages/password/PasswordScreen';

const ROUTES_CONFIG = {
	menu: {
		id: 0,
		name: 'menu',
		component: MenuScreen,
		title: 'Меню',
		isShowButton: false,
		options: { headerShown: false },
	},

	password: {
		id: 1,
		name: 'password',
		component: PasswordScreen,
		title: 'Пароль',
		isShowButton: true,
		options: { headerShown: false },
	},

	color: {
		id: 2,
		name: 'color',
		component: ColorScreen,
		title: 'Цвет',
		isShowButton: true,
		options: { headerShown: false },
	},
};

const ROUTES_ARRAY_DATA = Object.entries(ROUTES_CONFIG).map(
	([key, obj]) => obj
);

const ROUTES_ARRAY_BUTTON = ROUTES_ARRAY_DATA.filter(item => item.isShowButton);

export { ROUTES_CONFIG, ROUTES_ARRAY_DATA, ROUTES_ARRAY_BUTTON };
