import { createContext, useState } from 'react';
import COLORS from '../../../assets/styles/stylesColors';

const AppContext = createContext();

const initState = {
	bgColor: COLORS.bg_dark,
};

function AppProvider({ children }) {
	const [state, setState] = useState(initState);

	const changeColorBg = color => setState({ ...state, bgColor: color });

	const store = {
		...state,
		changeColorBg,
	};

	return <AppContext.Provider value={store}>{children}</AppContext.Provider>;
}

export { AppProvider, AppContext };
