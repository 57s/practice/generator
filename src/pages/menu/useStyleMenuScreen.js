import { StyleSheet } from 'react-native';

function useStyleMenuScreen() {
	const styles = StyleSheet.create({
		page: {
			flex: 1,
			justifyContent: 'flex-end',
		},

		list: {
			paddingVertical: 15,
			gap: 15,
		},
	});

	return styles;
}

export default useStyleMenuScreen;
