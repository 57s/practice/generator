import { View } from 'react-native';
import { useNavigation } from '@react-navigation/native';

import { ROUTES_ARRAY_BUTTON } from '../../providers/routes/routes-data';

import Layout from '../../components/layout/Layout';
import Button from '../../components/button/Button';

import useStyleMenuScreen from './useStyleMenuScreen';

function MenuScreen() {
	const navigation = useNavigation();
	const styles = useStyleMenuScreen();
	return (
		<Layout>
			<View style={styles.page}>
				<View style={styles.list}>
					{ROUTES_ARRAY_BUTTON.map(route => (
						<Button
							key={route.id}
							title={route.title}
							onPress={() => navigation.push(route.name)}
						/>
					))}
				</View>
			</View>
		</Layout>
	);
}

export default MenuScreen;
