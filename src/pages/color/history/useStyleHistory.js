import { StyleSheet } from 'react-native';

function useStyleHistory() {
	const styles = StyleSheet.create({
		history: {
			gap: 15,
		},
	});

	return styles;
}

export default useStyleHistory;
