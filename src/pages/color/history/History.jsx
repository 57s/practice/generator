import { useContext } from 'react';
import { View } from 'react-native';
import * as Clipboard from 'expo-clipboard';

import { AppContext } from '../../../providers/app/appProvider';

import Story from '../story/Story';

import useStyleHistory from './useStyleHistory';

const copyClipboard = async value => {
	await Clipboard.setStringAsync(value);
};

function History({ history, setHistory }) {
	const historyData = history.filter((item, index) => index > 0);
	const styles = useStyleHistory();
	const { changeColorBg } = useContext(AppContext);
	const setColorMessage = id => {
		const message = history.find(item => item.id === id);
		if (message) {
			changeColorBg(message.color);
		}
	};

	const copyColorMessage = id => {
		const message = history.find(item => item.id === id);
		copyClipboard(message.color);
	};

	const deleteMessage = id => {
		setHistory(history.filter(message => message.id !== id));
	};

	if (historyData.length > 0) {
		return (
			<View style={styles.history}>
				{historyData.map(story => (
					<Story
						key={story.id}
						{...story}
						onSet={() => setColorMessage(story.id)}
						onCopy={() => copyColorMessage(story.id)}
						onDelete={() => deleteMessage(story.id)}
					/>
				))}
			</View>
		);
	}
}

export default History;
