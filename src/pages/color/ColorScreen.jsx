import { useContext, useState } from 'react';
import { ScrollView, Text, View } from 'react-native';
import * as Clipboard from 'expo-clipboard';

import { generateHexColor, generateId } from '../../utils/utils';
import { AppContext } from '../../providers/app/appProvider';

import Layout from '../../components/layout/Layout';
import Box from '../../components/box/Box';
import Button from '../../components/button/Button';
import History from './history/History';
import Title from '../../components/title/Title';
import COLORS from '../../../assets/styles/stylesColors';
import useStyleColorScreen from './useStyleColorScreen';

const copyClipboard = async value => {
	await Clipboard.setStringAsync(value);
};

function ColorScreen() {
	const { changeColorBg, bgColor } = useContext(AppContext);
	const [colorText, setColorText] = useState(bgColor);
	const styles = useStyleColorScreen(bgColor);
	const [history, setHistory] = useState([]);

	const isShowCard = bgColor !== COLORS.bg_dark;

	const addStory = (colorBg, text) => {
		const newStory = {
			...colorBg,
		};
		setHistory([newStory, ...history]);
	};

	const genNewBg = () => {
		return {
			id: generateId(),
			color: generateHexColor(),
		};
	};

	const handlerCopy = () => {
		const bacColor = colorText;
		setColorText('Copied 😉');
		copyClipboard(colorText);
		setTimeout(() => setColorText(bacColor), 1500);
	};

	const handlerGenerate = () => {
		const newBg = genNewBg();
		addStory(newBg);
		changeColorBg(newBg.color);
	};

	return (
		<Layout>
			<View style={styles.page}>
				<ScrollView
					style={styles.scroll}
					contentContainerStyle={styles.scrollContainer}>
					<History history={history} setHistory={setHistory} />
				</ScrollView>

				<Box isShow={isShowCard} onPress={handlerCopy}>
					<Title>{colorText}</Title>
				</Box>

				<Button title="Генерировать" onPress={handlerGenerate} />
			</View>
		</Layout>
	);
}

export default ColorScreen;
