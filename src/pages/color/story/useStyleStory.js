import { StyleSheet } from 'react-native';
import COLORS from '../../../../assets/styles/stylesColors';

function useStyleStory(color) {
	const styles = StyleSheet.create({
		story: {
			flexDirection: 'row',
			justifyContent: 'space-between',
			alignItems: 'center',

			overflow: 'hidden',
			borderRadius: 15,
			borderWidth: 1,
			borderColor: COLORS.accent,
			elevation: 7,
			backgroundColor: COLORS.bg,
		},

		colorBox: {
			minHeight: 100,
			width: 100,
			backgroundColor: color,
		},

		main: {
			flex: 1,
			justifyContent: 'space-between',
			padding: 5,
		},

		title: {
			fontSize: 24,
			fontWeight: '800',
			textAlign: 'center',
			color: color,
		},

		control: {
			paddingRight: 15,
		},
	});

	return styles;
}

export default useStyleStory;
