import { useState } from 'react';
import { Text, View } from 'react-native';

import useStyleStory from './useStyleStory';
import ButtonIcon from '../../../components/buttonIcon/ButtonIcon';

function Story({ color, onCopy, onSet, onDelete }) {
	const styles = useStyleStory(color);
	const [colorText, setColorText] = useState(color);

	const handlerCopy = () => {
		const bacColor = colorText;
		setColorText('Copied 😉');
		onCopy();
		setTimeout(() => setColorText(bacColor), 1500);
	};

	return (
		<View style={styles.story}>
			<View style={styles.colorBox}></View>
			<View style={styles.main}>
				<Text style={styles.title}>{colorText}</Text>
			</View>

			<View style={styles.control}>
				<ButtonIcon name="brush" onPress={onSet} />
				<ButtonIcon name="clipboard" onPress={handlerCopy} />
				<ButtonIcon name="trash" onPress={onDelete} />
			</View>
		</View>
	);
}

export default Story;
