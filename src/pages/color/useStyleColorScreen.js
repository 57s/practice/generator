import { StyleSheet } from 'react-native';

function useStyleColorScreen(bgColor) {
	const styles = StyleSheet.create({
		page: {
			flex: 1,
			justifyContent: 'flex-end',
			gap: 15,
			paddingVertical: 15,
		},

		scroll: {
			flex: 1,
		},

		scrollContainer: {
			gap: 15,
			paddingVertical: 15,
		},
	});

	return styles;
}

export default useStyleColorScreen;
