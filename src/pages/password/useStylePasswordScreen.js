import { StyleSheet } from 'react-native';
import COLORS from '../../../assets/styles/stylesColors';

function useStylePasswordScreen() {
	const styles = StyleSheet.create({
		page: {
			flex: 1,
			justifyContent: 'flex-end',
			gap: 15,
			paddingVertical: 15,
		},

		scroll: {},

		scrollContainer: {
			gap: 15,
			paddingVertical: 15,
		},
	});

	return styles;
}

export default useStylePasswordScreen;
