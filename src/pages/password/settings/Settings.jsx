import { useState } from 'react';
import { Text, View } from 'react-native';

import Accordion from '../../../components/accordion/Accordion';
import Counter from '../../../components/counter/Counter';
import Checkbox from '../../../components/Checkbox/Checkbox';

import useStyleSettings from './useStyleSettings';

function Settings({ config, setConfig }) {
	const [isOpen, setOpen] = useState(false);
	const styles = useStyleSettings();

	const toggleOpen = () => setOpen(!isOpen);

	const changeUpperCaseChars = () =>
		setConfig({ ...config, isUpperCaseChars: !config.isUpperCaseChars });

	const changeLowerCaseChars = () =>
		setConfig({ ...config, isLowerCaseChars: !config.isLowerCaseChars });

	const changeDigitChars = () =>
		setConfig({ ...config, isDigitChars: !config.isDigitChars });

	const changeSpecialChars = () =>
		setConfig({ ...config, isSpecialChars: !config.isSpecialChars });

	const changeLength = value => {
		setConfig({ ...config, length: value });
	};

	const decrementLength = () => {
		setConfig({ ...config, length: --config.length });
	};

	const incrementLength = () => {
		setConfig({ ...config, length: ++config.length });
	};

	return (
		<Accordion title="Настройки" isOpen={isOpen} onToggle={toggleOpen}>
			<View style={styles.settings}>
				<View style={styles.lengthBox}>
					<Text style={styles.lengthTitle}>Длинна: </Text>
					<Counter
						count={config.length}
						onChange={changeLength}
						onIncrement={incrementLength}
						onDecrement={decrementLength}
					/>
				</View>

				<Checkbox
					title="Маленькие буквы:"
					isChecked={config.isLowerCaseChars}
					onPress={changeLowerCaseChars}
				/>

				<Checkbox
					title="Большие буквы:"
					isChecked={config.isUpperCaseChars}
					onPress={changeUpperCaseChars}
				/>

				<Checkbox
					title="Цифры:"
					isChecked={config.isDigitChars}
					onPress={changeDigitChars}
				/>

				<Checkbox
					title="Спец. символы:"
					isChecked={config.isSpecialChars}
					onPress={changeSpecialChars}
				/>
			</View>
		</Accordion>
	);
}

export default Settings;
