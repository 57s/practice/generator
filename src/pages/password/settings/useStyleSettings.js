import { StyleSheet } from 'react-native';
import COLORS from '../../../../assets/styles/stylesColors';

function useStyleSettings() {
	const styles = StyleSheet.create({
		settings: {
			gap: 15,
		},

		lengthBox: {
			flexDirection: 'row',
			alignItems: 'center',
			justifyContent: 'space-between',
			gap: 15,
		},

		lengthTitle: {
			fontSize: 20,
			color: COLORS.white,
		},
	});

	return styles;
}

export default useStyleSettings;
