import { useState } from 'react';
import { ScrollView, Text, View } from 'react-native';
import * as Clipboard from 'expo-clipboard';

import { generateString } from '../../utils/utils';

import Layout from '../../components/layout/Layout';
import Box from '../../components/box/Box';
import Button from '../../components/button/Button';
import Settings from './settings/Settings';
import Title from '../../components/title/Title';

import useStylePasswordScreen from './useStylePasswordScreen';

const initConfig = {
	length: 8,
	isUpperCaseChars: true,
	isLowerCaseChars: true,
	isDigitChars: true,
	isSpecialChars: false,
};

const copyClipboard = async value => {
	await Clipboard.setStringAsync(value);
};

function PasswordScreen() {
	const styles = useStylePasswordScreen();
	const [config, setConfig] = useState(initConfig);
	const [password, setPassword] = useState(null);

	const generatePassword = () => {
		setPassword(generateString(config));
	};

	const handlerPassword = () => {
		const bacPassword = password;
		setPassword('Copied 😉');
		copyClipboard(password);
		setTimeout(() => setPassword(bacPassword), 1500);
	};

	return (
		<Layout>
			<View style={styles.page}>
				<ScrollView
					style={styles.scroll}
					contentContainerStyle={styles.scrollContainer}>
					<Settings config={config} setConfig={setConfig} />
				</ScrollView>

				<Box isShow={password} onPress={handlerPassword}>
					<Title>{password}</Title>
				</Box>

				<Button title="Генерировать" onPress={generatePassword} />
			</View>
		</Layout>
	);
}

export default PasswordScreen;
