function generateId() {
	const data = new Date().getTime();
	const random = Math.random();

	return `id_${String(data)}_${random}`;
}

function generateRandomNumber(min = 0, max = 10) {
	return Math.floor(Math.random() * (max - min + 1)) + min;
}

const generateHexColor = () => {
	const hexRange = '0123456789ABCDEF';
	let color = '#';

	for (let index = 0; index < 6; index++) {
		color += hexRange[generateRandomNumber(0, hexRange.length - 1)];
	}

	return color;
};

function generateString(config) {
	const {
		length = 16,
		isUpperCaseChars = true,
		isLowerCaseChars = true,
		isDigitChars = true,
		isSpecialChars = false,
	} = config || {};

	const upperCaseChars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
	const lowerCaseChars = 'abcdefghijklmnopqrstuvwxyz';
	const digitChars = '0123456789';
	const specialChars = '!@#$%^&*()_+';

	let characterList = '';

	if (isUpperCaseChars) {
		characterList += upperCaseChars;
	}

	if (isLowerCaseChars) {
		characterList += lowerCaseChars;
	}

	if (isDigitChars) {
		characterList += digitChars;
	}

	if (isSpecialChars) {
		characterList += specialChars;
	}

	let string = '';

	for (let index = 0; index < length; index++) {
		string += characterList[generateRandomNumber(0, characterList.length - 1)];
	}

	return string;
}

export { generateId, generateRandomNumber, generateHexColor, generateString };
